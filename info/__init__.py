from flask import Flask, session, render_template
from flask_sqlalchemy import SQLAlchemy
from redis import StrictRedis
from flask_wtf.csrf import CSRFProtect, generate_csrf
from flask_session import Session
from config import config_dict
import logging
from logging.handlers import RotatingFileHandler
# from . import models
from info.utility.commont.filter import *

db = SQLAlchemy()
redis_store = None


def write_log(config_class):
    # 设置日志的记录等级
    logging.basicConfig(level=config_class.LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 10, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


def creat_app(config_dict_key):
    config_class = config_dict[config_dict_key]
    write_log(config_class)
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    global redis_store
    redis_store = StrictRedis(host=config_class.REDIS_HOST, port=config_class.REDIS_PORT, decode_responses=True)
    CSRFProtect(app)

    @app.after_request
    def set_csrf(response):
        csrf_token = generate_csrf()
        response.set_cookie('csrf_token', csrf_token)
        return response

    @app.errorhandler(404)
    @get_user
    def handler_404(err):
        user = g.user
        data = {
            'user_info': user.to_dict() if user else None
        }
        return render_template('news/404.html', data=data)

    app.add_template_filter(clicks_new_class, 'clicks_new_class')
    Session(app)
    from info.modules.index import index_bp
    from info.modules.passregister import passregister_bp
    from info.modules.news import news_bp
    from info.modules.user import user_bp
    from info.modules.admin import admin_bp
    app.register_blueprint(index_bp)
    app.register_blueprint(passregister_bp)
    app.register_blueprint(news_bp)
    app.register_blueprint(user_bp)
    app.register_blueprint(admin_bp)
    return app
