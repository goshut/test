function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}


$(function () {

    $(".pic_info").submit(function (e) {
        e.preventDefault();

        // 上传头像
        $(this).ajaxSubmit({
            url: "/user/push_avatar",
            type: "POST",
            headers: {
                "X-CSRFToken": getCookie('csrf_token')
            },
            success: function (resp) {
                alert('头像正在上传');
                if (resp.errno == "0") {
                    $(".now_user_pic").attr("src", resp.data.avatar_url);
                    $(".user_center_pic>img", parent.document).attr("src", resp.data.avatar_url);
                    $(".user_login>img", parent.document).attr("src", resp.data.avatar_url)
                    window.location.reload()
                }else {
                    alert(resp.errmsg)
                }
            }
        })
    })
});