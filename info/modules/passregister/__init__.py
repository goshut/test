from flask import Blueprint

passregister_bp = Blueprint('passregister', __name__, url_prefix='/passregister')

from .view import *