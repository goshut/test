import re
from flask import Blueprint

admin_bp = Blueprint('admin', __name__, url_prefix='/admin')

from .view import *


@admin_bp.before_request
def is_admin():
    if not (session.get('is_admin') or re.match('.*'+url_for('admin.admin_login')+'$', request.url)):
        return redirect(url_for('index.index'))


