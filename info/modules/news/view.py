from info import db
from info.models import Comment, News, CommentLike, User
from info.utility.commont.filter import get_user
from info.constants import CLICK_RANK_MAX_NEWS
from info.response_code import RET
from . import news_bp
from flask import render_template, session, current_app, jsonify, g, abort, request


@news_bp.route('/<int:news_id>')
@get_user
def detail(news_id):
    user = g.user
    user_dict = user.to_dict() if user else None
    from info.models import News
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    click_news_list = []
    for new in news_list if news_list else []:
        click_news_list.append(new.to_dict())
    new_detail = None
    try:
        new_detail = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        abort(404)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")

    # 准备新闻评论数据
    comment_list = []
    try:
        comments = new_detail.comments.order_by(Comment.create_time.desc())
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    for comment in comments:
        comment_list.append(comment.to_dict())



    # 获取当前新闻的评论
    comments = None
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)

    comment_like_ids = []
    if g.user:
        # 如果当前用户已登录
        try:
            comment_ids = [comment.id for comment in comments]
            if len(comment_ids) > 0:
                # 取到当前用户在当前新闻的所有评论点赞的记录
                comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comment_ids),
                                                         CommentLike.user_id == g.user.id).all()
                # 取出记录中所有的评论id
                comment_like_ids = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

    comment_list = []
    for item in comments if comments else []:
        comment_dict = item.to_dict()
        comment_dict["is_like"] = False
        # 判断用户是否点赞该评论
        if g.user and item.id in comment_like_ids:
                comment_dict["is_like"] = True
        comment_list.append(comment_dict)

    # is_collected = False
    # # 判断用户是否收藏过该新闻
    # if g.user:
    #     if news in g.user.collection_news:
    #         is_collected = True
    # data = {
    #     "news": news.to_dict(),
    #     "click_news_list": click_news_list,
    #     "is_collected": is_collected,
    #     "user_info": g.user.to_dict() if g.user else None,
    #     "comments": comment_list
    # }
    # return render_template('news/detail.html', data=data)






    """判断是否收藏"""




    # 是否关注过这个作者
    # 当前登录用户是否关注当前新闻作者
    is_followed = False
    # 判断用户是否收藏过该新闻
    is_collected = False

    # print(new_detail)
    # print(type(new_detail))
    # print(new_detail.user_id)
    # print(type(new_detail.user))
    # print(new_detail.user.followers)
    # print(type(new_detail.user.followers))
    if g.user:
        if new_detail in g.user.collection_news:
            is_collected = True
        if new_detail.user:
            if new_detail.user.followers.filter(User.id == g.user.id).count() > 0:
                is_followed = True
        else:
            is_followed = 'fuck!!!'




    new_dict = new_detail.to_dict() if new_detail else None
    if new_detail:
        new_detail.clicks += 1
    data = {
        'user_info': user_dict,
        'click_news_list': click_news_list,
        'new_detail': new_dict,
        "comments": comment_list,
        "is_collected": is_collected,
        "is_followed": is_followed
    }
    return render_template('news/detail.html', data=data)


@news_bp.route('/comment', methods=['POST'])
@get_user
def comment_push():
    from info.models import News, Comment
    data = request.json
    content = data.get('comment')
    parent_id = data.get('parent_id')
    news_id = data.get('news_id')
    user = g.user
    if not all([content, news_id, user]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数不全')
    try:
        new = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
    if not new:
        return jsonify(errno=RET.NODATA, errmsg='没有这条新闻')
    comment = Comment()
    comment.news_id = new.id
    comment.user_id = user.id
    comment.content = content
    if parent_id:
        comment.parent_id = parent_id
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="评论保存错误")
    return jsonify(errno=RET.OK, errmsg="评论成功", data=comment.to_dict())


@news_bp.route("/news_collect", methods=['POST'])
@get_user
def news_collect():
    """新闻收藏"""

    user = g.user
    json_data = request.json
    news_id = json_data.get("news_id")
    action = json_data.get("action")

    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    if not news_id:
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("collect", "cancel_collect"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

    if not news:
        return jsonify(errno=RET.NODATA, errmsg="新闻数据不存在")

    if action == "collect":
        user.collection_news.append(news)
    else:
        user.collection_news.remove(news)

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="保存失败")
    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_bp.route('/comment_like', methods=["POST"])
@get_user
def set_comment_like():
    """评论点赞"""

    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    # 获取参数
    comment_id = request.json.get("comment_id")
    news_id = request.json.get("news_id")
    action = request.json.get("action")

    # 判断参数
    if not all([comment_id, news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("add", "remove"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询评论数据
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据失败")

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg="评论数据不存在")

    if action == "add":
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if not comment_like:
            comment_like = CommentLike()
            comment_like.comment_id = comment_id
            comment_like.user_id = g.user.id
            db.session.add(comment_like)
            # 增加点赞条数
            comment.like_count += 1
    else:
        # 删除点赞数据
        comment_like = CommentLike.query.filter_by(comment_id=comment_id, user_id=g.user.id).first()
        if comment_like:
            db.session.delete(comment_like)
            # 减小点赞条数
            comment.like_count -= 1

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg="操作失败")
    return jsonify(errno=RET.OK, errmsg="操作成功")


@news_bp.route('/followed_user', methods=["POST"])
@get_user
def followed_user():
    """关注/取消关注用户"""
    if not g.user:
        return jsonify(errno=RET.SESSIONERR, errmsg="用户未登录")

    user_id = request.json.get("user_id")
    action = request.json.get("action")

    if not all([user_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    if action not in ("follow", "unfollow"):
        return jsonify(errno=RET.PARAMERR, errmsg="参数错误")

    # 查询到关注的用户信息
    try:
        target_user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="查询数据库失败")

    if not target_user:
        return jsonify(errno=RET.NODATA, errmsg="未查询到用户数据")

    # 根据不同操作做不同逻辑
    if action == "follow":
        if target_user.followers.filter(User.id == g.user.id).count() > 0:
            return jsonify(errno=RET.DATAEXIST, errmsg="当前已关注")
        target_user.followers.append(g.user)
    else:
        if target_user.followers.filter(User.id == g.user.id).count() > 0:
            target_user.followers.remove(g.user)

    # 保存到数据库
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据保存错误")

    return jsonify(errno=RET.OK, errmsg="操作成功")
