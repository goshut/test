from flask import session, current_app, jsonify, g
from info.response_code import RET
from functools import wraps


def clicks_new_class(index):
    if index == 1:
        return 'first'
    elif index == 2:
        return 'second'
    elif index == 3:
        return 'third'
    else:
        return ''


def get_user(fun):
    @wraps(fun)
    def wrapper(*args, **kwargs):
        user_id = session.get('user_id')
        user = None
        from info.models import User
        if user_id:
            try:
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)
                return jsonify(errno=RET.DBERR, errmsg="数据库查询错误")
        g.user = user
        return fun(*args, **kwargs)
    return wrapper
