from redis import StrictRedis
import logging


class Config:
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/test'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    SESSION_TYPE = 'redis'
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=1)
    SECRET_KEY = 'DSFADSFASFASDFSA3151564ASF'
    SESSION_USE_SIGNER = True
    SESSION_PERMANENT = False
    PERMANENT_SESSION_LIFETIME = 86400


class TestConfig(Config):
    DEBUG = True
    LOG_LEVEL = logging.DEBUG


class MatureConfig(Config):
    DEBUG = False
    LOG_LEVEL = logging.ERROR


config_dict = {
    'test': TestConfig,
    'mature': MatureConfig
}
