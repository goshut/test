from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from info import creat_app, db, redis_store
import logging
from info.models import User

app = creat_app('test')
manager = Manager(app)
Migrate(app, db)
manager.add_command('db', MigrateCommand)


@manager.option('-n', '--name', dest='name')
@manager.option('-p', '--password', dest='password')
def create_admin(name, password):
    if not all([name, password]):
        print('参数不足')
        return
    user = User()
    user.mobile = name
    user.nick_name = name
    user.password = password
    user.is_admin = True
    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        print('创建失败')
    print('创建成功')


if __name__ == '__main__':
    manager.run()
